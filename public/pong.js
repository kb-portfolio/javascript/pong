const canvas = document.querySelector('canvas');

canvas.addEventListener('click', function () {
    game_start = !game_start;
})

const ctx = canvas.getContext('2d');

canvas.width = 1000
canvas.height = 500;

//stan gry 
let game_start = false;

//pole
const cw = canvas.width;
const ch = canvas.height;

//pilka
const ballSize = 20;
let ballX = cw / 2 - ballSize / 2;
let ballY = ch / 2 - ballSize / 2;

//paletki
const paddleHeight = 100;
const paddleWidth = 20;

const playerX = 70;
const aiX = 910;

let playerY = 200;
let aiY = 200;

//linie na planszy
const lineWidth = 6;
const lineHight = 16;

//predkosc 
let ballSpeedX = -5;
let ballSpeedY = 4;

//score

let PlayerScore = 0;
let AiScore = 0;

ctx.fillRect(cw / 2, ch / 2, ballSize, ballSize);


function table() {

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, cw, ch);


    ctx.fillStyle = 'gray';
    for (let linePosition = 20; linePosition < ch; linePosition += 30) {
        ctx.fillRect(cw / 2 - lineWidth / 2, linePosition, lineWidth, lineHight);
    }

}

function ball() {

    ctx.fillStyle = 'white';
    ctx.fillRect(ballX, ballY, ballSize, ballSize);

    ballX += ballSpeedX;
    ballY += ballSpeedY;

    if (ballY <= 0 || ballY + ballSize >= ch) {
        ballSpeedY = -ballSpeedY;
        speedUp();
    }

    if (ballX <= 0 ) {
        ballSpeedX = -ballSpeedX;
        AiScore++;
        document.querySelector('.score2').innerHTML = AiScore + " !!";
    } else if (ballX + ballSize >= cw){
        ballSpeedX = -ballSpeedX;
        PlayerScore++;
        document.querySelector('.score1').innerHTML = PlayerScore + " !!";
    }
}

function player() {
    ctx.fillStyle = '#7fff00';
    ctx.fillRect(playerX, playerY, paddleWidth, paddleHeight);
}

function AI() {
    ctx.fillStyle = 'yellow';
    ctx.fillRect(aiX, aiY, paddleWidth, paddleHeight);
}


let topCanvas = canvas.offsetTop;


function playerPosition(e) {

    playerY = e.offsetY - paddleHeight / 2;

    if (playerY >= ch - paddleHeight) {
        playerY = ch - paddleHeight;
    }

    if (playerY <= 0) {
        playerY = 0;
    }

    // aiY = playerY;
}

function speedUp() {

    // Prędkość X
    if (ballSpeedX > 0 && ballSpeedX <= 16) {
        ballSpeedX += 0.1;
    } else if (ballSpeedX <= 0 && ballSpeedX >= -16) {
        ballSpeedX -= 0.1;
    }

    // Prędkość Y

    if (ballSpeedY > 0 && ballSpeedY <= 6) {
        ballSpeedY += 0.1;
    } else if (ballSpeedY <= 0 && ballSpeedY >= -6) {
        ballSpeedY -= 0.1;
    }
}


function aiPosition() {
    var middlePaddel = aiY + paddleHeight / 2;
    var middleBall = ballY + ballSize / 2;
    var SwitchAIMode = 0.7;
    var speedMultiples = 1;
    if (ballSpeedX > 6 && ballSpeedX < 9.49) {
        SwitchAIMode = 0.6;
        var speedMultiples = 1.2;
    } else if (ballSpeedX > 9.5) {
        SwitchAIMode = 0.55;
        var speedMultiples = 1.4;
    }
    if (ballX > 0.7*cw){
        if(ballY < 0.2*ch || ballY > 0.8*ch){
            speedMultiples *= 1.5;
        }
    }

    // AI od połowy do 3/4 mapy pilka sie zbliza
    if (ballX > cw / 2 && ballSpeedX > 0 && ballX < SwitchAIMode * cw) {

        if (ballSpeedY < 0) {
            AIUp(middlePaddel, middleBall, speedMultiples);
        }
        if (ballSpeedY > 0) {
            AIDown(middlePaddel, middleBall, speedMultiples);
        }

        // AI od 3/4 mapy  mapy pilka sie zbliza
    } else if (ballX > SwitchAIMode * cw && ballSpeedX > 0) {
        AIUp(middlePaddel, middleBall, speedMultiples);
        AIDown(middlePaddel, middleBall, speedMultiples);

        // Ruch po odbiciu
    } else if (ballX > cw*0.86 && ballSpeedX < 0) {
        AIUp(middlePaddel, middleBall, speedMultiples);
        AIDown(middlePaddel, middleBall, speedMultiples);

        // AI od połowy  mapy pilka sie oddalaa 
        // paletka wraca na srodek   
    } else if (ballX > cw / 2) {
        if (middlePaddel > ch / 2 + 50) {
            aiY -= 3;
        } else if (middlePaddel < ch / 2 - 50) {
            aiY += 3;
        }

        // AI po stronie przeciwnika  
    } else if (ballX <= cw / 2 && ballX > cw / 4 && ballSpeedX > 0) {

        if (middlePaddel - middleBall > 200) {
            aiY += 4;
        } else if (middlePaddel - middleBall > 100) {
            aiY += 4;

        } else if (middlePaddel - middleBall < -200) {
            aiY -= 4;

        } else if (middlePaddel - middleBall < -100) {
            aiY -= 4;

        }
    }
    AIMaxMinPos();
}

function AIDown(middlePaddel, middleBall, speedMultiples) {
    if (middlePaddel - middleBall < -150) {
        aiY += speedMultiples * 12;
    } else if (middlePaddel - middleBall < -70) {
        aiY += speedMultiples * 7;
    } else if (middlePaddel - middleBall < -15) {
        aiY += speedMultiples * 4;
    }
}

function AIUp(middlePaddel, middleBall, speedMultiples) {

    if (middlePaddel - middleBall > 150) {
        aiY -= speedMultiples *12;
        console.log("12");
    } else if (middlePaddel - middleBall > 70) {
        aiY -= speedMultiples *7;
        console.log("7");
    } else if (middlePaddel - middleBall > 15) {
        aiY -= speedMultiples *4;
        console.log("4");
    }
}

function AIMaxMinPos() {
    if (aiY < 0) {
        aiY = 0;
    } else if (aiY + paddleHeight > ch) {
        aiY = ch - paddleHeight;
    }
}

function paddleColision(paddlePosX) {

    var middleBallY = ballY + ballSize / 2;
    var middleBallX = ballX + ballSize / 2;
    let colisionLine = 0;

    if (paddlePosX < ch / 2) {
        colisionLine = paddlePosX + paddleWidth;
        middlePaddelY = playerY + paddleHeight / 2;

        if (ballSpeedX < 0 && ballX <= colisionLine) {

            if (middleBallY + ballSize / 2 >= middlePaddelY - paddleHeight / 2 && middleBallY - ballSize / 2 <= middlePaddelY + paddleHeight / 2) {
                ballSpeedX = -ballSpeedX;

                // console.log("middleBallX "+middleBallX+" middleBallY " + middleBallY + " middlePaddelY- paddleHeight/2 "+ (middlePaddelY- paddleHeight/2));
            }
        }

    } else {
        colisionLine = paddlePosX;
        middlePaddelY = aiY + paddleHeight / 2;

        if (ballSpeedX > 0 && ballX + ballSize >= colisionLine) {

            if (middleBallY + ballSize / 2 >= middlePaddelY - paddleHeight / 2 && middleBallY - ballSize / 2 <= middlePaddelY + paddleHeight / 2) {
                ballSpeedX = -ballSpeedX;

                // console.log("middleBallX "+middleBallX+" middleBallY " + middleBallY + " middlePaddelY- paddleHeight/2 "+ (middlePaddelY- paddleHeight/2));
            }
        }
    }
}

canvas.addEventListener('mousemove', playerPosition)

function load() {
    table();
    ball();
    player();
    AI();
}

function game() {
    if (game_start) {
        table();
        ball();
        player();
        AI();
        aiPosition();
        paddleColision(playerX);
        paddleColision(aiX);
    }

}

load();
setInterval(game, 1000 / 60);